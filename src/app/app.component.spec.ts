import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AuthenticationService} from './services/authentication.service'
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { of } from 'rxjs';
import { USER } from './core/mock';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;  

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AuthenticationService,
        RouterModule,
        HttpClient,
        HttpHandler
      ]
    }).compileComponents();  

    spyOnProperty(AuthenticationService.prototype, 'currentUser', 'get').and.returnValue(of(USER));
    
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {    
    expect(component).toBeTruthy();
  });  

  it('should check the Login', () => {    
    fixture.detectChanges();

    expect(component.currentUser).toEqual(USER);
  });  
});
