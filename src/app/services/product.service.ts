import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from './../core/constants';
import { Product } from '../models/product';

@Injectable({ providedIn: 'root' })
export class ProductService {
    constructor(private http: HttpClient) {
    }

    save(product: Product) {
        return this.http.post<any>(`${CONFIG.apiUrl}/product`, { product });
    }

    getTableDataAndConfiguration(): any {
        return this.http.get<any>(`${CONFIG.apiUrl}/product`);
    }
}