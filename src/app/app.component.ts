import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { User } from './models/user';

@Component({
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  currentUser!: User;
  loggedIn = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.authenticationService.currentUser.subscribe((user: User) => { 
      this.currentUser = user;
      if (this.currentUser && this.currentUser.id > 0) {
        this.loggedIn = true;
      } 
    });
  }

  logout() {
    this.loggedIn = false;
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  newProduct() {
    this.router.navigate(['/new-product']);
  }

  sales() {
    this.router.navigate(['/sales']);
  }
}
