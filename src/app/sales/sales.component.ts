import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';
import { Table } from 'primeng/table';

@Component({
    templateUrl: './sales.component.html',
    styleUrls: ['./sales.component.scss'],
})

export class SalesComponent implements OnInit {
    @ViewChild('dt') dt!: Table;

    newProductForm: FormGroup;
    products!: Product[];
    tableColumns: any;
    subColumns: any;
    destroy$ = new Subject();

    constructor(
        private formBuilder: FormBuilder,
        private productService: ProductService,
    ) {
        this.newProductForm = this.formBuilder.group({
            productId: ['', Validators.required],
            productName: ['', Validators.required],
            productManager: ['', Validators.required],
            salesStartDate: ['', Validators.required],
        });
    }

    ngOnInit() {
        this.productService.getTableDataAndConfiguration().subscribe((p: any) => {
            this.products = p.data;
            this.tableColumns = p.column;
            this.subColumns = p.column.find((col: any) => col.subHeaders).subHeaders;
        });
    }

    ngOnDestroy() {
        if (this.destroy$) {
            this.destroy$.next();
            this.destroy$.complete();
        }
    }

    clear(table: Table) {
        table.clear();
    }

    applyFilterGlobal($event: any, stringVal: any) {
        this.dt.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
    }

    newRow() {
        return { productID: '', productName: '', salesQ1: 0, salesQ2: 0, salesQ3: 0, salesQ4: 0 };
    }
}