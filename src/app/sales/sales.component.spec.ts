import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';
import { SalesComponent } from './sales.component';
import { ProductService } from '../services/product.service';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { of } from 'rxjs';
import { PRODUCT_TABLE } from './../core/mock';

describe('SalesComponent', () => {
  let fixture: ComponentFixture<SalesComponent>;
  let component: SalesComponent;  

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        SalesComponent
      ],
      providers: [        
        ProductService,
        RouterModule,
        HttpClient,
        HttpHandler,
        FormBuilder,
      ]
    }).compileComponents();  

    spyOn(ProductService.prototype, 'getTableDataAndConfiguration').and.returnValue(of(PRODUCT_TABLE));
    
    fixture = TestBed.createComponent(SalesComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {    
    expect(component).toBeTruthy();
  });  

  it('should get all Products', () => {    
    fixture.detectChanges();

    expect(component.products).toEqual(PRODUCT_TABLE.data);
    expect(component.tableColumns).toEqual(PRODUCT_TABLE.column);
  });  
});
