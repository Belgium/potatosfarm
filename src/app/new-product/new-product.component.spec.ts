import { TestBed, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { NewProductComponent } from './new-product.component';
import { ProductService } from '../services/product.service';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CalendarModule } from 'primeng/calendar';
import { of } from 'rxjs';

fdescribe('SalesComponent', () => {
  let fixture: ComponentFixture<NewProductComponent>;
  let component: NewProductComponent;  
  let productServiceSpy: jasmine.SpyObj<ProductService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,        
        ReactiveFormsModule,
        CalendarModule,
      ],
      declarations: [
        NewProductComponent
      ],
      providers: [        
        ProductService,
        RouterModule,
        HttpClient,
        HttpHandler,
        FormBuilder,
      ]
    }).compileComponents();    
    
    productServiceSpy = TestBed.get(ProductService);
    
    fixture = TestBed.createComponent(NewProductComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {    
    expect(component).toBeTruthy();
  });  

  it('should post the new product', () => {    
    spyOn(ProductService.prototype, 'save').and.returnValue(of({}));

    fixture.detectChanges();
    component.onSubmit();
    
    expect(productServiceSpy.save).toHaveBeenCalledTimes(1);
  });  
});
