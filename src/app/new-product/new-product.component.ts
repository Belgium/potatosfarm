import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ProductService } from '../services/product.service';

@Component({
    templateUrl: './new-product.component.html',
    styleUrls: ['./new-product.component.scss']
})

export class NewProductComponent implements OnInit, OnDestroy {
    newProductForm: FormGroup;
    formValid = false;
    submitted = false;
    destroy$ = new Subject();

    constructor(
        private formBuilder: FormBuilder,
        private productService: ProductService,
    ) {
        this.newProductForm = this.formBuilder.group({
            productId: ['', [Validators.required, Validators.maxLength(13)]],
            productName: ['', [Validators.required, Validators.maxLength(50)]],
            productManager: ['', [Validators.maxLength(30)]],
            salesStartDate: [null, [Validators.required, Validators.maxLength(10)]],
        });
    }

    ngOnInit() {
        this.newProductForm.statusChanges
            .pipe(takeUntil(this.destroy$))
            .subscribe(status => this.formValid = (status === "VALID"));
    }

    ngOnDestroy() {
        if (this.destroy$) {
            this.destroy$.next();
            this.destroy$.complete();
        }
    }

    onSubmit() {
        this.productService.save(this.newProductForm.value).subscribe();
        this.newProductForm.reset();
    }  
}