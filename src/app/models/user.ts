export interface User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token?: string;
}

export const INITIAL_USER: User = {
    id: 0,
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    token: '',
}