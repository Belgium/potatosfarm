import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from './../services/authentication.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'login-app',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  destroy$ = new Subject();
  formValid = false;
  loading = false;
  submitted = false;
  returnUrl = '';
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.authenticationService.logout();
    this.loginForm.statusChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(status => this.formValid = (status === "VALID"));
  }

  ngOnDestroy() {
    if (this.destroy$) {
      this.destroy$.next();
      this.destroy$.complete();
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm && this.loginForm.valid) {
      this.authenticationService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)
        .pipe(first())
        .subscribe(
          () => {
            this.router.navigate(['/']);
          },
          (error) => {
            this.error = error;
          });
    }
  }
}
